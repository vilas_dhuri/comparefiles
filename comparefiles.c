#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
int compare_file(FILE *fp1, FILE *fp2) {
    unsigned long position;
    char c1, c2;
    for (position = 0;; position++) {
        c1 = getc(fp1);
        c2 = getc(fp2);
        if (c1 != c2 || c1 == EOF || c2 == EOF)
            break;
    }
    if (c1 == c2) {
        printf("Files are identical");
        return 0;  // files are identical
    } else
    if (c1 == EOF) {
        printf("\nFile1 is included in File2, the first %lu bytes are identical\n", position);       //First file is a subset of Second File
        printf("\nBut they differ at differ at position %lu: 0x%02X & 0x%02X\n", position, c1, c2);
        return 1;
    } 
    else if (c2 == EOF) {
        printf("File2 is included in File1, the first %lu bytes are identical\n", position);         //Second file is a subset of first file
        printf("\nBut they differ at position %lu: 0x%02X & 0x%02X\n", position, c1, c2);
        return 2;
    } 
    else {
        printf("\nFile1 and Fgile2 differ at position %lu: 0x%02X & 0x%02X\n", position, c1, c2);
        return 2;
    }
}

struct timeval start, stop;
int main(int argc, char **argv){
    
    double amount_of_time = 0; 
    FILE *fp1, *fp2;
    if (argc < 3)
    {
        printf("\nInsufficient Arguments:\n");
            return 0;
    }
    
    fp1 = fopen(argv[1], "rb");
    fp2 = fopen(argv[2], "rb");

    if (fp1 == NULL || fp2 == NULL)
    {
        printf("ERROR: FILE NOT FOUND!!");
        return 0;
    }
    else
    {
        gettimeofday(&start, NULL);
        compare_file(fp1,fp2);
        gettimeofday(&stop, NULL);
        amount_of_time = (double)(stop.tv_usec - start.tv_usec) / 1000000 + (double)(stop.tv_sec - start.tv_sec);
        printf ("\nTime Taken to read is: %.6f ms",amount_of_time);
    }
    fclose(fp1);
    fclose(fp2);
    return 0;
}
