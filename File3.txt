Developing memory-efficient systems is a long-standing goal of modern technologies. 
The rise in demand for embedded-system computing platforms accompanied by the global chip shortage has led to the further development of system-on-a-chip architectures. 
With the increase in chip demand, chip microprocessors (CMP) are becoming mainstream. 
The ability to provide on-chip multithreading and multitasking to achieve faster data processing and higher speed of operation has brought up a new challenge in designing the architecture of these new processors. 
Embedded microprocessors play an important role in this type of development due to their lightweight operating system cost.